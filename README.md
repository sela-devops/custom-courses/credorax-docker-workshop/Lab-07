# Docker Workshop
Lab 07: Managing Containers

---

## Preparations

 - Create a new folder for the lab:
```
$ mkdir ~/lab-07
$ cd ~/lab-07
```

## Instructions

 - Run the following containers using dockerhub images:
```
$ docker run -d -p 5002:5002 -e "port=5002" --name python-app-5002 selaworkshops/python-app:2.0
```
```
$ docker run -d -p 5003:5003 -e "port=5003" --name python-app-5003 selaworkshops/python-app:2.0
```

 - Ensure the containers are running:
```
$ docker ps
```

 - Stop the first container:
```
$ docker stop python-app-5002
```

 - Kill the second container:
```
$ docker kill python-app-5003
```

 - Display running containers:
```
$ docker ps
```

 - Show all the containers (includind non running containers):
```
$ docker ps -a
```

 - Let's start both containers again:
```
$ docker start python-app-5002 python-app-5003
```

 - Restart the second container:
```
$ docker restart python-app-5002
```

 - Display the docker host information with:
```
$ docker info
```

 - Show the running processes in the first container using:
```
$ docker top python-app-5002
```

 - Display the resource usage statistics of both containers (exit with Ctrl + C):
```
$ docker stats python-app-5002 python-app-5003
```

 - Retrieve the history of the second container image:
```
$ docker history selaworkshops/python-app:2.0
```

 - Inspect the second container image:
```
$ docker inspect selaworkshops/python-app:2.0
```

 - Inspect the first container and look for the internal ip:
```
$ docker inspect python-app-5002
```
```
"Networks": {
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "822cb66790c6358d9decab874916120f3bdeff7193a4375c94ca54d50832303d",
                    "EndpointID": "9aa96dc29be08eddc6d8f429ebecd2285c064fda288681a3611812413cbdfc1f",
                    "Gateway": "172.17.0.1",
                    "IPAddress": "172.17.0.3",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:11:00:03",
                    "DriverOpts": null
                }
            }
```

 - Show the logs of the second container using the flag --follow:
```
$ docker logs --follow python-app-5003
```

 - Browse to the application and see the containers logs from the terminal:
```
http://<server-ip>:5003
```

 - Stop to tracking logs:
 ```
$ CTRL + C
```

 - Inspect the content of the current directory:
 ```
$ ls -l
```

 - Copy some files from the second container to the host:
 ```
$ docker cp python-app-5003:/app/index.html .
```

 - Inspect the content of the current directory:
 ```
$ ls -l
```

 - Replace the index.html content using the command below
```
echo "<h1>Updated HTML</h1>" > index.html
```

 - Inspect the index.html file content
```
cat index.html
```

 - Copy the updated index.html file into the another container
```
docker cp index.html python-app-5002:/app/index.html
```

 - Browse to the application to see the new content
```
http://<server-ip>:5002
```

## Cleanup

 - Cleanup the environment:
```
$ docker rm -f $(docker ps -a -q)
$ docker rmi -f $(docker images -q)
```

